mkdir install
cd install

wget https://github.com/davidemms/OrthoFinder/releases/download/2.3.12/OrthoFinder.tar.gz
wget http://eddylab.org/software/hmmer/hmmer.tar.gz
wget https://github.com/arq5x/bedtools2/releases/download/v2.25.0/bedtools-2.25.0.tar.gz
git clone https://gitlab.com/xonq/OrthoFiller.git
git clone https://github.com/vipints/GFFtools-GX.git
git clone https://github.com/Gaius-Augustus/Augustus.git
