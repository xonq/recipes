# NEED to add source command
# NEED to fix repeatmodeler path (BuildDatabase not working)

# start with miniconda3 as build environment
FROM continuumio/miniconda3 AS build

# Update, install mamba and conda-pack:
RUN conda update -n base -c defaults --yes conda && \
    conda install -c conda-forge -n base --yes mamba conda-pack

# Install funannotate deps from bioconda
# here specifying specific versions to be able to set ENV below
RUN mamba create -c conda-forge -c bioconda -c defaults \
    -n funannotate --yes "python>=3.6,<3.9" biopython xlrd==1.2.0 \
    "trinity==2.8.5" "evidencemodeler==1.1.1" "pasa==2.4.1" "codingquarry==2.0" \
    "proteinortho==6.0.16" goatools matplotlib-base natsort numpy pigz \
    pandas psutil requests "scikit-learn<1.0.0" scipy seaborn "blast=2.2.31" \
    tantan bedtools hmmer exonerate "diamond>=2.0.5" tbl2asn blat "trnascan-se>=2.0" \
    ucsc-pslcdnafilter trimmomatic raxml iqtree trimal "mafft>=7" hisat2 \
    trf repeatmodeler snap fasta3 \
    "kallisto==0.46.1" minimap2 stringtie "salmon>=0.9" "samtools>=1.9" \
    glimmerhmm bamtools perl perl-yaml perl-file-which perl-local-lib perl-dbd-mysql perl-clone perl-hash-merge \
    perl-soap-lite perl-json perl-logger-simple perl-scalar-util-numeric perl-math-utils perl-mce \
    perl-text-soundex perl-parallel-forkmanager perl-db-file \
    perl-perl4-corelibs ete3 distro \
    && conda clean -a -y

# Since we want the most recent, install from repo, remove snap as broken
SHELL ["conda", "run", "-n", "funannotate", "/bin/bash", "-c"]
RUN python -m pip install git+https://github.com/nextgenusfs/funannotate.git

# package with conda-pack
RUN conda-pack --ignore-missing-files -n funannotate -o /tmp/env.tar && \
    mkdir /venv && cd /venv && tar xf /tmp/env.tar && \
    rm /tmp/env.tar

# We've put venv in same path it'll be in final image
RUN /venv/bin/conda-unpack

# Now build environment
FROM debian:buster AS runtime

# Copy /venv from the previous stage:
COPY --from=build /venv /venv
COPY libssl1.0.0_1.0.2l-1~bpo8+1_amd64.deb libssl1.0.0
COPY multiarch-support_2.28-10_amd64.deb multiarch

RUN dpkg -i multiarch && rm multiarch && \
    dpkg -i libssl1.0.0 && rm libssl1.0.0

# Install debian snap via apt-get
RUN apt-get update && apt-get install -y locales locales-all libgl1 procps \
    augustus augustus-data libssl1.0.0 && \
    rm -rf /var/lib/apt/lists/* && \
    rm "/venv/bin/fasta" && \
    ln -s "/venv/bin/fasta36" "/venv/bin/fasta"
#    ln -s /usr/bin/snap-hmm /usr/bin/snap && \


# add it to the PATH and add env variables
ENV PATH="/venv/bin:$PATH" \
    EVM_HOME="/venv/opt/evidencemodeler-1.1.1" \
    PASAHOME="/venv/opt/pasa-2.4.1" \
    TRINITYHOME="/venv/opt/trinity-2.8.5" \
    QUARRY_PATH="/venv/opt/codingquarry-2.0/QuarryFiles" \
    ZOE="/usr/share/snap" \
    USER="me" \
    TRF_PRGM="/venv/bin/trf" \
    HMMER_DIR="/venv/bin/"

#    FUNANNOTATE_DB="/opt/databases"
#    AUGUSTUS_CONFIG_PATH="/usr/share/augustus/config" \

# When image is run, run the code with the environment
SHELL ["/bin/bash", "-c"]
CMD funannotate

WORKDIR /xonq/

COPY RepeatMasker /xonq/RepeatMasker

RUN python3 -m pip install h5py ete3

RUN cd /xonq/RepeatMasker && \
    perl ./configure -hmmer_dir /venv/bin/ && \
    funannotate check

WORKDIR /xonq/data

ENTRYPOINT /bin/bash
