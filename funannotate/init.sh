wget -O libssl1.0.0_1.0.2l-1~bpo8+1_amd64.deb \
http://snapshot.debian.org/archive/debian/20170705T160707Z/pool/main/o/openssl/libssl1.0.0_1.0.2l-1%7Ebpo8%2B1_amd64.deb
wget -O multiarch-support_2.28-10_amd64.deb \
http://snapshot.debian.org/archive/debian/20190501T215844Z/pool/main/g/glibc/multiarch-support_2.28-10_amd64.deb
wget https://dfam.org/releases/Dfam_3.7/families/Dfam_curatedonly.h5.gz
git clone https://github.com/rmhubley/RepeatMasker.git
git clone https://github.com/Dfam-consortium/RepeatModeler.git
gunzip Dfam_curatedonly.h5.gz
mv Dfam_curatedonly.h5 RepeatMasker/Libraries/Dfam.h5
