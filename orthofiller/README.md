# BUILDING

1) clone the repository:
```
git clone https://gitlab.com/xonq/recipes/orthofiller.git
```

2) get the source files:
```
cd orthofiller
bash getsrc.sh
```

3) build the container:
```
sudo docker build -t <USR/REPO>:<VERSION> .
```

# RUNTIME

If you do not have root access - say you are using Singularity as your
container manager - you will not be able to add species configurations to the
default Augustus configuration path. Instead, you will have to edit `source.sh`
with a writeable `AUGUSTUS_CONFIG_PATH`. This can be appended to your
`.bash_profile`/`.bashrc` or alternatively called at runtime via `source
source.sh`. 
